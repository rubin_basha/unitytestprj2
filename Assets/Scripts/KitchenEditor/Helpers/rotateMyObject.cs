﻿using UnityEngine;
using System.Collections;

public class rotateMyObject : MonoBehaviour {

	public bool rotateIsActive;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (rotateIsActive) {

			GameObject temp = GameObject.Find ("Sphere");

			Debug.Log ("Rotate");
			this.transform.rotation *= Quaternion.AngleAxis(30.0f * Time.deltaTime, Vector3.up);
			transform.RotateAround (temp.transform.position, Vector3.up, 20 * Time.deltaTime);
		}
	}

}

