﻿using UnityEngine;
using System.Collections;

public class touchHandler : MonoBehaviour {

	Color originalColor;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseEnter() {
		originalColor = this.GetComponent<Renderer> ().material.color;
		this.GetComponent<Renderer> ().material.color = Color.green;
	}

	void OnMouseExit() {
		this.GetComponent<Renderer> ().material.color = originalColor;
	}

}
