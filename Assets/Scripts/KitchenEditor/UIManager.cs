﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;

public class UIManager : MonoBehaviour {

	//UI
	public GameObject Panel_Menu;

	List<GameObject> myObis = new List<GameObject>();
	bool menuIsVisible;
	bool rotateIsActive = false;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void addCube() {
		GameObject myItem = Instantiate(Resources.Load("Kitchen01/Cube")) as GameObject;
		myItem.name = "cubi" + myObis.Count;
		myItem.transform.position = new Vector3 (Random.Range(-2, 2), Random.Range(-2, 2), Random.Range(-2, 2));
		myItem.GetComponent<Renderer> ().material.color = new Color (Random.value, Random.value, Random.value);
		myObis.Add (myItem);
	}

	public void offCube() {
		foreach (GameObject tempObis in myObis) {
			tempObis.SetActive (false);
		}
	}

	public void onCube() {
		foreach (GameObject tempObis in myObis) {
			tempObis.SetActive (true);
		}
	}

	public void moveObisLeft() {
		foreach (GameObject tempObis in myObis) {
			tempObis.transform.position = new Vector3 (tempObis.transform.position.x + 2, tempObis.transform.position.y, tempObis.transform.position.z);
		}
	}

	public void moveObisRight() {
		foreach (GameObject tempObis in myObis) {
			tempObis.transform.position = new Vector3 (tempObis.transform.position.x - 2, tempObis.transform.position.y, tempObis.transform.position.z);
		}
	}

	public void rotateObjects() {
		rotateIsActive = !rotateIsActive;
		foreach (GameObject tempObis in myObis) {
			tempObis.GetComponent<rotateMyObject> ().rotateIsActive = rotateIsActive;

		}
	}

	public void changeMaterial() {
		foreach (GameObject tempObis in myObis) {
			tempObis.GetComponent<Renderer> ().material = Resources.Load("Styles/modernStyle") as Material;
		}
	}

	public void deleteKitchen() {
		foreach (GameObject tempObis in myObis) {
			Destroy (tempObis);
		}
		myObis.Clear ();
	}

	public void hideKitchen() {
		foreach (GameObject tempObis in myObis) {
			tempObis.SetActive(false);
		}
	}

	public void showKitchen() {
		foreach (GameObject tempObis in myObis) {
			tempObis.SetActive(true);
		}
	}

	public void showMenu() {
		if (menuIsVisible) {
			//Panel_Menu.SetActive (false);
			Panel_Menu.transform.DOMoveX(-135.0f,1.0f);
			menuIsVisible = false;
		} else {
			//Panel_Menu.SetActive (true);
			Panel_Menu.transform.DOMoveX(135.0f,1.0f);
			menuIsVisible = true;
		}

	}
}
